import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;



public class TestLibraryItem {
    
    /*
    This class tests all the functionalities with user capabilties
    There are 3 roles: 

    librarian - can add, remove items from the library
    registered user - can view, borrow, return items from the library
    guest - can only view and return items from the library 

    */

    public static void main(String[] args) {
    
        Library lib = new Library();

        //testing existing users
        User user1 =  new User("testRegisteredUser", UserStatus.REGISTERED);
        User user2 = new User("testGuestUser", UserStatus.GUEST);

        User librarian1 =  new User("testLibrarian1", UserStatus.LIBRARIAN);
        User librarian2 = new User("testLibrarian2", UserStatus.LIBRARIAN);

        lib.userLog.put(1, user1);
        lib.userLog.put(2, user2);

        lib.librarianLog.put(1, librarian1);
        lib.librarianLog.put(2, librarian2);

        HashMap<String, String> detailTest = new HashMap<>();
        detailTest.put("author", "Annie");
        detailTest.put("summary", "A book about a lion");
        lib.addItem(ItemType.BOOK, "The Lion", detailTest);

        HashMap<String, String> detailTest3 = new HashMap<>();
        detailTest3.put("author", "Ian Jukes");
        detailTest3.put("summary", "A book about furries");
        LibraryItem item2;
        item2 = new Books("A Whole New Work: Furries");
        ((Details) item2).setDetails(detailTest3);
        lib.libraryInventory.put(1111, item2);

        HashMap<String, String> detailTest4 = new HashMap<>();
        detailTest4.put("publisher", "Frank C");
        detailTest4.put("year", "2020");
        LibraryItem item3;
        item3 = new Periodicals("Javascript is Like Java, but While Smoking A Joint");
        ((Details) item3).setDetails(detailTest4);
        lib.libraryInventory.put(1222, item3);


        Scanner typeOfUser = new Scanner(System.in);
        int user;

        Scanner userIDCheck = new Scanner(System.in);
        int userID;

        System.out.println("Are you a \n1: Libarian \n2: User \n3: Guest");

        try {
            user = typeOfUser.nextInt();
            if(user == 1){
                System.out.println("What is your librarian ID");
                userID = typeOfUser.nextInt();
                if(lib.userLog.containsKey(userID)){
                    System.out.println("Hello Librarian: " + lib.librarianLog.get(userID).getName());
                    Scanner userAction = new Scanner(System.in);
                    int action;
                    do {
                        System.out.println("1: Add item\n2: Remove item\n3: Show Inventory\n4: Leave Library");
                       
                        action = userAction.nextInt();

                        switch(action){
                            case 1:
                                
                                Scanner libAddType = new Scanner(System.in);
                                int addType;

                                ItemType type = null;

                                HashMap<String, String> details = new HashMap<String, String>();


                                try {
                                    
                                    do{
                                        System.out.println("What item are you adding?\n1:Book\n2:CD\n3:DVD\n4:Periodical");
                                        addType = libAddType.nextInt();
                                        
                                        switch(addType){
                                            case 1:
                                                type = ItemType.BOOK;
                                                
                                                break;
                                            case 2:
                                                type = ItemType.CD;
                                           
                                                break;
                                            case 3:
                                                type = ItemType.DVD;
                                             
                                                break;
                                            case 4:
                                                type = ItemType.PERIODICAL;
                                           
                                                break;

                                        }
                                       

                                    }while(addType < 1 || addType > 4);
                                    
                                    System.out.println("What is the title of this item?");
                                    Scanner libAddName = new Scanner(System.in);
                                    String addName;
                                    addName =  libAddName.nextLine();

                                    details = lib.getDetails(type);

                                    lib.addItem(type, addName, details);

                                } catch (InputMismatchException e) {
                                    //TODO: handle exception
                                    System.out.println("Not a number option!");
                                }

                                break;
                            case 2:
                              if(lib.libraryInventory.size() != 0){
                                Scanner libRemoveType = new Scanner(System.in);
                                int removeNum;
                    
                                System.out.println("What item are you removing?");
                                lib.showInventory();
                                removeNum = libRemoveType.nextInt();

                                lib.removeItem(removeNum);
                              }else{
                                  System.out.println("Inventory is empty!");
                              }
                            
                                break;
                            case 3:
                                lib.showInventory();
                                break;
                            default:
                                System.out.println("Not an option!");
                        }
            
                    } while (action != 4 || action < 1 || action > 4);
                }else{
                    System.out.println("Librarian does not exist");
                }
            }else
            if(user == 2){
                System.out.println("What is your user ID");
                userID = typeOfUser.nextInt();
                if(lib.userLog.containsKey(userID)){
                    System.out.println("Hello User: " + lib.userLog.get(userID).getName());
                    Scanner userAction = new Scanner(System.in);
                    int action;
                    do {
                        System.out.println("1: Borrow item\n2: Return item\n3: View Item\n4: Leave Library");
                       
                        action = userAction.nextInt();

                        switch(action){
                            case 1:

                                Scanner libBorrowType = new Scanner(System.in);
                                int borrowKey;

                                System.out.println("Which item would you like to borrow?");
                                lib.showInventory();

                                borrowKey = libBorrowType.nextInt();

                                lib.borrowItem(borrowKey);

                                break;

                            case 2:
                                Scanner libReturnType = new Scanner(System.in);
                                int returnKey;

                                System.out.println("Which item would you like to return?");
                                lib.showInventory();
                                returnKey = libReturnType.nextInt();
                               

                                lib.returnItem(returnKey);
                                break;

                            case 3:
                                Scanner libViewType = new Scanner(System.in);
                                int viewKey;

                                System.out.println("Which item would you like to view?");
                                lib.showInventory();
                                viewKey = libViewType.nextInt();
                                

                                lib.viewItem(viewKey);
                                break; 
                            default:
                                System.out.println("Not an option!");
                        }
            
                
                    } while (action != 4 || action < 1 || action > 4);
                }else{
                    System.out.println("User does not exist");
                }
            }else
            if(user == 3){
                System.out.println("Hello Guest");
                Scanner userAction = new Scanner(System.in);
                int action;
                do {
                    System.out.println("1: Return Item\n2: View Item\n3: Leave Library");
                       
                    action = userAction.nextInt();

                    
                    switch(action){
  
                        case 1:
                            Scanner libReturnType = new Scanner(System.in);
                            int returnKey;

                            System.out.println("Which item would you like to return?");
                            lib.showInventory();
                            returnKey = libReturnType.nextInt();
                           
                            lib.returnItem(returnKey);

                            break;

                        case 2:
                            Scanner libViewType = new Scanner(System.in);
                            int viewKey;

                            System.out.println("Which item would you like to view?");
                            lib.showInventory();
                            viewKey = libViewType.nextInt();
                            

                            lib.viewItem(viewKey);
                            break; 
                        default:
                            System.out.println("Not an option!");
                    }
            
                
                } while (action != 3);

            }else{
                System.out.println("Not a valid user");
            }
        } catch (InputMismatchException e) {
            System.out.println("Not a number!");
        }

        
    }
}