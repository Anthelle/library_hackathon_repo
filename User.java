public class User {
    String name;
	UserStatus status;
	
	public User(String name, UserStatus status) {
		this.name = name;
		this.status = status;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public UserStatus getStatus() {
		return status;
	}
	public void setStatus(UserStatus status) {
		this.status = status;
    }
    
}