public enum BorrowStatus {
    BORROWED,
    NOTBORROWED,
    CANTBORROW
}