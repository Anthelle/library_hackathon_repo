import java.util.HashMap;

public class TestLibrary {
    /*

    This class tests all the functionalities in a static way 


    */
    public static void main(String[] args) {
        Library lib = new Library();

        HashMap<String, String> detailTest = new HashMap<>();
        detailTest.put("author", "Annie");
        detailTest.put("summary", "A book about a lion");
        lib.addItem(ItemType.BOOK, "The Lion", detailTest);
 

        HashMap<String, String> detailTest2 = new HashMap<>();
        detailTest2.put("author", "Will");
        detailTest2.put("summary", "A book about a bird");
        LibraryItem item1;
        item1 = new Books("A BIRD BOOK");
        ((Details) item1).setDetails(detailTest2);
        lib.libraryInventory.put(1000, item1);

        HashMap<String, String> detailTest3 = new HashMap<>();
        detailTest3.put("author", "Ian Jukes");
        detailTest3.put("summary", "A book about furries");
        LibraryItem item2;
        item2 = new Books("A Whole New Work: Furries");
        ((Details) item2).setDetails(detailTest3);
        lib.libraryInventory.put(1111, item2);

        HashMap<String, String> detailTest4 = new HashMap<>();
        detailTest4.put("publisher", "Frank C");
        detailTest4.put("year", "2020");
        LibraryItem item3;
        item3 = new Periodicals("Javascript is Like Java, but While Smoking A Joint");
        ((Details) item3).setDetails(detailTest4);
        lib.libraryInventory.put(1222, item3);

 

        System.out.println("*****To show view item");
        lib.viewItem(1222);
        lib.showInventory();
        System.out.println("\n");
/*
       
        lib.showInventory();

        System.out.println("*****To show removing item");
        lib.removeItem(1000);
        lib.showInventory();
        System.out.println("\n");

        System.out.println("*****To show borrowing item");
        lib.borrowItem(1111);
        lib.showInventory();
        System.out.println("\n");

        System.out.println("*****To show borrowing not borrowable item");
        lib.borrowItem(1222);
        lib.showInventory();
        System.out.println("\n");

        System.out.println("*****To show view item");
        lib.viewItem(1222);
        lib.showInventory();
        System.out.println("\n");

        System.out.println("*****To show removing a borrowed item");
        lib.removeItem(1111);
        lib.showInventory();
        System.out.println("\n");

        System.out.println("*****To show returning a borrowed");
        lib.returnItem(1111);
        lib.showInventory();
        System.out.println("\n");

        System.out.println("*****To show returning a viewed item");
        lib.returnItem(1222);
        lib.showInventory();
        System.out.println("\n");

*/


        

    }
}