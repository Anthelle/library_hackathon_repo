public abstract class LibraryItem implements Details {
    ItemType itemType;
    Status status;
    BorrowStatus borrowStatus;
    String title;
  
    

    public LibraryItem(String title) {
        this.status = Status.AVAILABLE;
        this.title = title;
       
        setItemType();
    }

    public ItemType getItemType() {
        return itemType;
    }
    public abstract void setItemType();

    public Status getStatus() {
        return status;
    }

    public abstract void setStatus(Status s);

    public BorrowStatus getBorrowStatus() {
        return borrowStatus;
    }

    public abstract void setBorrowStatus(BorrowStatus s);


    
}