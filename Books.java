import java.util.HashMap;
import java.util.Map;

public class Books extends LibraryItem {

    String author;
    String summary;

    public Books(String title) {
        super(title);
        setBorrowStatus(BorrowStatus.NOTBORROWED);

    }

    @Override
    public void setStatus(Status s) {
        // TODO Auto-generated method stub
        this.status = s;

    }

    @Override
    public void setItemType() {
        // TODO Auto-generated method stub
        this.itemType = ItemType.BOOK;
    }

    @Override
    public void setBorrowStatus(BorrowStatus b) {
        // TODO Auto-generated method stub
        this.borrowStatus = b;

    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setDetails(HashMap<String, String> details) {
        // TODO Auto-generated method stub

       

        for (Map.Entry map : details.entrySet()) {
            String key = (String) map.getKey();
        
            switch (key) {
                case "author":
                    this.author = (String) details.get(key);
                    break;
                case "summary":
                    this.summary = (String) details.get(key);
                    break;
                default:
                    System.out.println("no match");

            }

        }

    }

    @Override
    public void getDetails() {
        // TODO Auto-generated method stub
        System.out.println("Author: " + author + "\nSummary: " + summary);

    }
  

}