import java.util.HashMap;
import java.util.Map;

public class DVDs extends LibraryItem {

    String director;

    public DVDs(String title) {
        super(title);
        setBorrowStatus(BorrowStatus.NOTBORROWED);
    }

    @Override
    public void setStatus(Status s) {
        // TODO Auto-generated method stub
        this.status = s;

    }

    @Override
    public void setItemType() {
        // TODO Auto-generated method stub
        this.itemType = ItemType.DVD;
    }

    @Override
    public void setBorrowStatus(BorrowStatus b) {
        // TODO Auto-generated method stub
        this.borrowStatus = b;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setDetails(HashMap<String, String> details) {
        // TODO Auto-generated method stub

        for (Map.Entry map : details.entrySet()) {
            String key = (String) map.getKey();
            switch (key) {
                case "director":
                    this.director = details.get(key);
                    break;

                default:
                    System.out.println("no match");

            }

        }

    }

    @Override
    public void getDetails() {
        // TODO Auto-generated method stub
        System.out.println("Director: " + director);

    }
 
}