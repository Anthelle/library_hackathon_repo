import java.util.HashMap;

public interface Details {
    public abstract void setDetails(HashMap<String,String> details) ;
    public abstract void getDetails();
}