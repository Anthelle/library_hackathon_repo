import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class Library{
    HashMap<Integer,LibraryItem> libraryInventory = new HashMap<Integer,LibraryItem>();
    HashMap<Integer, User> userLog = new HashMap<Integer, User>();
    HashMap<Integer, User> librarianLog = new HashMap<Integer, User>();


    private int getTotalItems(){

        return libraryInventory.size();

    }
    /*
        Shows currently existing users
    */
    public void showUserLog(){
        System.out.println("***User Log***");
        for (Integer userID : userLog.keySet())
        {
            User userInfo = userLog.get(userID);

            System.out.println(userID + " " + userInfo.name);
 
        }

    }
    /*
        Shows currently existing librarians
    */
    public void showLibrarianLog(){
        System.out.println("***Librarian Log***");
        for (Integer librarianID : librarianLog.keySet())
        {
            User librarianInfo = librarianLog.get(librarianID);

            System.out.println(librarianID + " " + librarianInfo.name);
 
        }
   

    }

    /*
        Method is used in the main class to request further details on item when added to the library
    */
    public HashMap<String, String> getDetails(ItemType t){
        HashMap<String, String> details = new HashMap<String, String>();

        switch(t){
            case BOOK:
                System.out.println("Author:");
                Scanner authorScan = new Scanner(System.in);
                String author = authorScan.nextLine();

                System.out.println("Summary:");
                Scanner summaryScan = new Scanner(System.in);
                String summary = summaryScan.nextLine();

                details.put("author", author);
                details.put("summary", summary);

                break;
            case CD:
                System.out.println("Artist:");
                Scanner artistScan = new Scanner(System.in);
                String artist = artistScan.nextLine();


                details.put("artist", artist);
              
                break;
            case PERIODICAL:
                System.out.println("Publisher:");
                Scanner publisherScan = new Scanner(System.in);
                String publisher = publisherScan.nextLine();

                System.out.println("Year:");
                Scanner yearScan = new Scanner(System.in);
                String year = yearScan.nextLine();

                details.put("publisher", publisher);
                details.put("year", year);
                break;
            case DVD:
                System.out.println("Director:");
                Scanner directorScan = new Scanner(System.in);
                String director = directorScan.nextLine();

                details.put("director", director);
                break;
        }

        return details;
    }

    /*
        Method adds items to the library hashmap
    */
    public void addItem(ItemType type, String title, HashMap <String, String> details){

      
        Random rand = new Random();
        int key = 0;

        do{
            key = rand.nextInt(100);
        }while(libraryInventory.containsKey(key));
       
        
        LibraryItem item1;
        switch(type){
            case BOOK:
                item1 = new Books(title);
                ((Details) item1).setDetails(details);
                libraryInventory.put(key, item1);
                break;
            case CD:
                item1 = new CDs(title);
                libraryInventory.put(key, item1);
                ((Details) item1).setDetails(details);
                break;
            case DVD:
                item1 = new DVDs(title);
                libraryInventory.put(key, item1);
                ((Details) item1).setDetails(details);
                break;
            case PERIODICAL:
                item1= new Periodicals(title);
                libraryInventory.put(key, item1);
                ((Details) item1).setDetails(details);
                break;
            
        }
  
    }

    /*
        Method removes an item from the library hashmap
        restricts removing a currently used item
    */
    public void removeItem(int key){
        if(libraryInventory.containsKey(key)){
            if(libraryInventory.get(key).borrowStatus != BorrowStatus.BORROWED || libraryInventory.get(key).status != Status.UNAVAILABLE){
                System.out.println("Item: " + key + ", title: " + libraryInventory.get(key).title);
                libraryInventory.remove(key);
                System.out.println("!!!Item removed");
            }else{
                System.out.println("Item: " + key + " is currently in use, cannot remove");
            }
        }else{
            System.out.println("Item: " + key + " does not exist!");
        }
    }

    /*
        Method allows user to borrow an item from the library hashmap
        restricts borrowing a currently unavailable/borrowed item
    */
    public void borrowItem(int key){
        if(libraryInventory.containsKey(key)){
            if(libraryInventory.get(key).status == Status.AVAILABLE && libraryInventory.get(key).borrowStatus == BorrowStatus.NOTBORROWED){
                System.out.println("Item: " + key + ", title: " + libraryInventory.get(key).title);
                libraryInventory.get(key).setStatus(Status.UNAVAILABLE);
                libraryInventory.get(key).setBorrowStatus(BorrowStatus.BORROWED);
                System.out.println("!!!Item borrowed");
            }else{
                System.out.println("Item: " + key + " cannot be borrowed/unavailable");
            }
        }else{
            System.out.println("Item: " + key + " does not exist!");
        }
    }

    /*
        Method allows user to view (not borrow) an item from the library hashmap
        restricts viewing an unavailable/borrowed book 
    */
    public void viewItem(int key){
        if(libraryInventory.containsKey(key)){
            if(libraryInventory.get(key).status == Status.AVAILABLE){
                System.out.println("Item: " + key + ", title: " + libraryInventory.get(key).title);
                libraryInventory.get(key).setStatus(Status.UNAVAILABLE);
                System.out.println("!!!Item Viewed");
                libraryInventory.get(key).getDetails();
            }else{
                System.out.println("Item: " + key + " cannot be viewed because it is unavailable");
            }
        }else{
            System.out.println("Item: " + key + " does not exist!");
        }
    }

    /*
        Method allows user to return (not borrow) an item from the library hashmap
        restricts returning a non library item
    */
    public void returnItem(int key){
        if(libraryInventory.containsKey(key)){
            if(libraryInventory.get(key).status == Status.UNAVAILABLE){
                System.out.println("Item: " + key + ", title: " + libraryInventory.get(key).title);
                libraryInventory.get(key).setStatus(Status.AVAILABLE);

                if(libraryInventory.get(key).borrowStatus == BorrowStatus.BORROWED){
                    libraryInventory.get(key).setBorrowStatus(BorrowStatus.NOTBORROWED);
                }
                System.out.println("!!!Item Returned");
            }else{
                System.out.println("Item: " + key + " is not returnable");
            }
        }else{
            System.out.println("Item: " + key + " does not exist!");
        }
    }



    public void showInventory(){

        if(getTotalItems() > 0){

            System.out.println("***Inventory***");
            for (Integer itemNumber : libraryInventory.keySet())
            {
                LibraryItem item = libraryInventory.get(itemNumber);

                System.out.println("Item ID: " + itemNumber + "\n" + item.getItemType() + " - " + item.title + "       " + item.getStatus());
    
            }
        }else{
            System.out.println("Inventory is Empty!!");
        }
   
    }

    
}