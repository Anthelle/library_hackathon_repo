import java.util.HashMap;
import java.util.Map;

public class Periodicals extends LibraryItem {
    String publisher;
    String year;

    public Periodicals(String title) {
        super(title);
        setBorrowStatus(BorrowStatus.CANTBORROW);
    }

    @Override
    public void setStatus(Status s) {
        // TODO Auto-generated method stub
        this.status = s;

    }

    @Override
    public void setItemType() {
        // TODO Auto-generated method stub
        this.itemType = ItemType.PERIODICAL;
    }

    @Override
    public void setBorrowStatus(BorrowStatus b) {
        // TODO Auto-generated method stub
        this.borrowStatus = b;

    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setDetails(HashMap<String, String> details) {
        // TODO Auto-generated method stub
        for (Map.Entry map : details.entrySet()) {
            String key = (String) map.getKey();
            switch (key) {
                case "publisher":
                    this.publisher = details.get(key);
                    break;
                case "year":
                    this.year = details.get(key);
                    break;

                default:
                    System.out.println("no match");

            }

        }
    }

    @Override
    public void getDetails() {
        // TODO Auto-generated method stub
        System.out.println("Publisher: " + publisher + "\nYear Published: " + year);
    }
    

    
}