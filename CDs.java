import java.util.HashMap;
import java.util.Map;

public class CDs extends LibraryItem {

    String artist;

    public CDs(String title) {
        super(title);
        setBorrowStatus(BorrowStatus.NOTBORROWED);
    }

    @Override
    public void setStatus(Status s) {
        // TODO Auto-generated method stub
        this.status = s;

    }

    @Override
    public void setItemType() {
        // TODO Auto-generated method stub
        this.itemType = ItemType.CD;
    }

    @Override
    public void setBorrowStatus(BorrowStatus b) {
        // TODO Auto-generated method stub
        this.borrowStatus = b;

    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setDetails(HashMap<String, String> details) {
        // TODO Auto-generated method stub

        for (Map.Entry map : details.entrySet()) {
            String key = (String) map.getKey();
            switch (key) {
                case "artist":
                    this.artist = details.get(key);
                    break;
                default:
                    System.out.println("no match");

            }

        }

    }

    @Override
    public void getDetails() {
        // TODO Auto-generated method stub
        System.out.println("Artist: " + artist);

    }

   
}